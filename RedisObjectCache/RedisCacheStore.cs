﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StackExchange.Redis;

namespace RedisObjectCache
{
    internal sealed class RedisCacheStore
    {
        private readonly IDatabase _redisDatabase;
        private readonly IServer _redisServer;
        private readonly ISubscriber _subscriber;
        private readonly JsonSerializerSettings _jsonSerializerSettings;

        internal RedisCacheStore(IDatabase redisDatabase, IServer redisServer, ISubscriber rediSubscriber)
        {
            _redisDatabase = redisDatabase;
            _redisServer = redisServer;
            _subscriber = rediSubscriber;

            _jsonSerializerSettings = new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = new RedisJsonContractResolver()
            };
        }

        internal object Set(RedisCacheEntry entry)
        {
            var ttl = GetTtl(entry.State);

            var valueJson = JsonConvert.SerializeObject(entry.Value, _jsonSerializerSettings);
            var stateJson = JsonConvert.SerializeObject(entry.State, _jsonSerializerSettings);

            _redisDatabase.StringSet(entry.Key, valueJson, ttl);
            _redisDatabase.StringSet(entry.StateKey, stateJson, ttl);

            return entry.Value;
        }

        internal object Get(string key)
        {
            var redisCacheKey = new RedisCacheKey(key);

            var stateJson = _redisDatabase.StringGet(redisCacheKey.StateKey);
            if (string.IsNullOrEmpty(stateJson))
                return null;

            var valueJson = _redisDatabase.StringGet(redisCacheKey.Key);
            var state = JsonConvert.DeserializeObject<RedisCacheEntryState>(stateJson);

            var value = GetObjectFromString(valueJson, state.TypeName);

            if (state.IsSliding)
            {
                state.UpdateUsage();
                stateJson = JsonConvert.SerializeObject(state, _jsonSerializerSettings);

                var ttl = GetTtl(state);
                _redisDatabase.StringSet(redisCacheKey.StateKey, stateJson, ttl);
                _redisDatabase.KeyExpire(redisCacheKey.Key, ttl);
            }

            return value;
        }

        internal object Remove(string key)
        {
            var redisCacheKey = new RedisCacheKey(key);
            var valueJson = _redisDatabase.StringGet(redisCacheKey.Key);
            if (string.IsNullOrEmpty(valueJson))
                return null;

            var value = JsonConvert.DeserializeObject(valueJson);

            _redisDatabase.KeyDelete(redisCacheKey.Key);
            _redisDatabase.KeyDelete(redisCacheKey.StateKey);

            return value;
        }

        private TimeSpan GetTtl(RedisCacheEntryState state)
        {
            return state.UtcAbsoluteExpiration.Subtract(DateTime.UtcNow);
        }

        private object GetObjectFromString(string json, string typeName)
        {
            MethodInfo method = typeof(JsonConvert).GetMethods().FirstOrDefault(m => m.Name == "DeserializeObject" && m.IsGenericMethod);
            var t = Type.GetType(typeName);
            MethodInfo genericMethod = method.MakeGenericMethod(t);
            return genericMethod.Invoke(null, new object[]{ json }); // No target, no arguments
        }

        ////////////////// Additions

        /// <summary>
        /// Scan keys using a patern (Carfull: this command might work unexpectedly if a cluster exists)
        /// </summary>
        /// <param name="pattern">Match pattern</param>
        /// <returns>Lsit of matching keys</returns>
        internal IEnumerable<string> ScanKeys(string pattern)
        {
            if (string.IsNullOrEmpty(pattern)) return null;
            if (!_redisServer.IsConnected) throw new ArgumentException("Server is not connected");

            return _redisServer.Keys(pattern: pattern).Select(k => k.ToString()).Where(k => !k.Contains("_"));
        }

        internal IEnumerable<ClientInfo> ClientList()
        {
            if (!_redisServer.IsConnected) throw new ArgumentException("Server is not connected");

            return _redisServer.ClientList();
        }

        internal void ClientKill(ClientInfo client)
        {
            // ToDo: Make this method more convenient
            if (!_redisServer.IsConnected) throw new ArgumentException("Server is not connected");

            _redisServer.ClientKill(client.Address);
        }

        internal void Subscribe(string channel, Action<RedisChannel, RedisValue> messageHandler)
        {
            _subscriber.Unsubscribe(channel);
            _subscriber.Subscribe(channel, messageHandler);
        }

        internal void Unsubscribe(string channel)
        {
            _subscriber.Unsubscribe(channel);
        }

        internal void Unsubscribe()
        {
            _subscriber.UnsubscribeAll();
        }

        internal void Publish(string channel, string message)
        {
            _subscriber.Publish(channel, message);
        }
    }
}
