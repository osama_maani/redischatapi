﻿using RedisObjectCache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

namespace RedisChatLib
{
    public class Operation
    {
        private const string UserIDPrefix = "U:";
        private const string UserTokenPrefix = "T:";
        private const string MessagePrefix = "M:";

        public static Response Auth(string userID, string password)
        {
            try
            {
                if (String.IsNullOrEmpty(userID) || string.IsNullOrEmpty(password))
                    return new Response
                    {
                        Success = false,
                        Message = "Invalid user data"
                    };

                if (RedisCache.Default.Get($"{UserIDPrefix}{userID}") is User user)
                {
                    if (password != Decrypt(user.Password))
                        return new Response
                        {
                            Success = false,
                            Message = "Wrong Password"
                        };

                    RedisCache.Default.Add($"{UserTokenPrefix}{userID}", new Guid().ToString(), new CacheItemPolicy
                    {
                        AbsoluteExpiration = DateTimeOffset.MaxValue,
                        SlidingExpiration = new TimeSpan(0, 30, 0)
                    });

                    return new Response
                    {
                        Success = true,
                        Message = "Access Granted"
                    };
                }
                
                return new Response
                {
                    Success = false,
                    Message = "User ID does not exist"
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    Success = false,
                    Message = $"An error has occured Ex: {e.Message}"
                };
            }
        }

        public static Response Signup(string userID, string password)
        {
            try
            {
                if (String.IsNullOrEmpty(userID) || string.IsNullOrEmpty(password))
                    return new Response
                    {
                        Success = false,
                        Message = "Invalid user data"
                    };

                if (RedisCache.Default.Contains($"{UserIDPrefix}{userID}"))
                    return new Response
                    {
                        Success = false,
                        Message = "User ID already exist"
                    };

                RedisCache.Default.Set($"{UserIDPrefix}{userID}", new User
                {
                    ID = userID,
                    Password = Encrypt(password)
                }, DateTimeOffset.MaxValue);

                RedisCache.Default.Add($"{UserTokenPrefix}{userID}", new Guid().ToString(), new CacheItemPolicy
                {
                    AbsoluteExpiration = DateTimeOffset.MaxValue,
                    SlidingExpiration = new TimeSpan(0, 30, 0)
                });

                return new Response
                {
                    Success = true,
                    Message = $"User {userID} Created successfuly"
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    Success = false,
                    Message = $"An error has occured Ex: {e.Message}"
                };
            }
        }

        private static string Encrypt(string password)
        {
            if (string.IsNullOrEmpty(password)) return password;
            return string.Join(";", Encoding.ASCII.GetBytes(password).Select(x => x ^ 7));
        }

        private static string Decrypt(string password)
        {
            if (string.IsNullOrEmpty(password)) return password;
            return Encoding.ASCII.GetString(password.Split(';').Select(x => (byte) (Convert.ToByte(x) ^ 7)).ToArray());
        }

        public static Response PostMessage(string userID, string message)
        {
            if (string.IsNullOrEmpty(userID) || string.IsNullOrEmpty(message))
                throw new ArgumentException("UserID, Message cannot be empty");

            if (RedisCache.Default.Contains($"{UserTokenPrefix}{userID}"))
            {
                RedisCache.Default.Add($"{MessagePrefix}{DateTime.Now.Ticks}", new Message
                {
                    Body = message,
                    TimeStamp = DateTime.Now.Ticks,
                    UserName = userID
                }, DateTimeOffset.MaxValue);

                return new Response
                {
                    Success = true
                };
            }

            return new Response
            {
                Success = false,
                Message = "User session timed out, new auth token is required"
            };
        }

        public static IEnumerable<Message> GetMessages(string userID)
        {
            try
            {
                if (RedisCache.Default.Contains($"{UserTokenPrefix}{userID}"))
                {
                    return RedisCache.Default.GetKeys($"{MessagePrefix}*")
                        .Select(message => (Message) RedisCache.Default.Get(message)).ToList();
                }
            
                return new List<Message>
                {
                    new Message
                    {
                        Body = "User session timed out, new auth token is required",
                        TimeStamp = DateTime.Now.Ticks,
                        UserName = "Redis-Lib"
                    }
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            //var messages = new List<Message>();
            //var keys = RedisCache.Default.GetKeys($"{MessagePrefix}*");
            //foreach (var message in keys)
            //{
            //    messages.Add((Message)RedisCache.Default.Get(message));
            //}
            //return messages;
        }

        public static object Remove(string key)
        {
            return RedisCache.Default.Remove(key);
        }

        public static void Subscribe( /*string channel*/)
        {
            RedisCache.Default.OnMessageRecieved -= OnMessageRecieved;
            RedisCache.Default.OnMessageRecieved += OnMessageRecieved;
            RedisCache.Default.Subscribe();
        }

        public static void UnsubscribeAll()
        {
            RedisCache.Default.UnsubscribeAll();
        }

        private static void OnMessageRecieved(string channel, string message)
        {
            Console.WriteLine($"* OnMessageRecieved:\n- Channel: {channel}, Message: {message}");
        }

        public static void Publish(string channel, string message)
        {
            RedisCache.Default.Publish(channel, message);
        }
    }
}