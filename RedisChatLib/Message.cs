﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisChatLib
{
    public class Message
    {
        public string UserName { get; set; }
        public string Body { get; set; }
        public long TimeStamp { get; set; }
    }
}
