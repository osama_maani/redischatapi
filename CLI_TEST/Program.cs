﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLI_TEST
{
    class Program
    {
        static void Main(string[] args)
        {
            string userID = null;
            string x = "auth";

            RedisChatLib.Operation.UnsubscribeAll();

            loop:

            if (userID != null)
            {
                Console.WriteLine("Commands: auth, sign, get, add, remove, sub, pub ...");
                Console.WriteLine("Exit (y)?");
                x = Console.ReadLine();
            }

            switch (x)
            {
                case "get":
                    foreach (var message in RedisChatLib.Operation.GetMessages(userID))
                    {
                        Console.WriteLine(
                            $"User: {message.UserName}, @ {new DateTime(message.TimeStamp)}, Message: {message.Body}");
                    }

                    goto loop;
                case "add":
                    Console.Write("Message: ");
                    var value = Console.ReadLine();
                    var ares = RedisChatLib.Operation.PostMessage(userID, value);
                    Console.WriteLine(ares.Success ? "OK" : ares.Message);
                    goto loop;
                case "remove":
                    Console.WriteLine(RedisChatLib.Operation.Remove(userID));
                    goto loop;
                case "auth":
                {
                    Console.Write("UserID: ");
                    userID = Console.ReadLine();
                    Console.Write("Password: ");
                    var aRes = RedisChatLib.Operation.Auth(userID, Console.ReadLine());
                    Console.WriteLine(aRes.Success ? "Authenticated" : aRes.Message);
                    goto loop;
                }
                case "sign":
                {
                    Console.Write("UserID: ");
                    var aU = Console.ReadLine();
                    Console.Write("Password: ");
                    var aRes = RedisChatLib.Operation.Signup(aU, Console.ReadLine());
                    Console.WriteLine(aRes.Success ? "Authenticated" : aRes.Message);
                    goto loop;
                }
                case "sub":
                    Console.WriteLine("Subscribed");
                    RedisChatLib.Operation.Subscribe( /*channel: Console.ReadLine()*/);
                    goto loop;
                case "pub":
                    // Console.Write("Channel: ");
                    var ch = "messages"; //Console.ReadLine();
                    Console.Write("Message: ");
                    RedisChatLib.Operation.Publish(channel: ch, message: Console.ReadLine());
                    goto loop;
                default:
                    if (x != "y")
                    {
                        goto loop;
                    }

                    break;
            }
        }
    }
}