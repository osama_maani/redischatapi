﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RedisChatLib;

namespace RedisChatWebApi.Controllers
{
    public class MessagingController : ApiController
    {
        public int PostMessage(string userName, string message)
        {
            return Operation.PostMessage(userName, message).Success ? 1 : 0;
        }

        public IEnumerable<Message> GetAllMessages(string userName)
        {
            try
            {
                return Operation.GetMessages(userName);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}