﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RedisChatWebApi.Controllers
{
    public class UserController : ApiController
    {
        [HttpGet]
        public int Login(string userName, string password)
        {
            return RedisChatLib.Operation.Auth(userName, password).Success ? 1 : 0;
        }

        [HttpGet]
        public int SignUp(string userName, string password)
        {
            return RedisChatLib.Operation.Signup(userName, password).Success ? 1 : 0;
        }
    }
}
